/*
 * Copyright (c) 2017. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package debop.examples.jpa.config;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.ConnectionReleaseMode;
import org.hibernate.cfg.Environment;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.hibernate4.HibernateExceptionTranslator;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.Properties;

/**
 * AbstractJpaConfiguration
 *
 * @author sunghyouk.bae@gmail.com
 * @since 17. 11. 10
 */
@Configuration
@EnableTransactionManagement(proxyTargetClass = true)
@Slf4j
public abstract class AbstractJpaConfiguration {

    abstract public String[] getMappedPackageNames();

    @Bean
    public DataSource dataSource() {
        return new EmbeddedDatabaseBuilder()
                .generateUniqueName(true)
                .setType(EmbeddedDatabaseType.H2)
                .build();
    }

    public Properties jpaProperties() {
        Properties props = new Properties();

        props.put(Environment.FORMAT_SQL, "true");
        props.put(Environment.HBM2DDL_AUTO, "create-drop"); // create | spawn | spawn-drop | update | validate | none

        props.put(Environment.SHOW_SQL, "true");
        props.put(Environment.RELEASE_CONNECTIONS, ConnectionReleaseMode.ON_CLOSE);
        props.put(Environment.AUTOCOMMIT, "true");
        props.put(Environment.STATEMENT_BATCH_SIZE, "100");

        //        props.put(Environment.CACHE_REGION_PREFIX, "hibernate:");

        // NOTE: Naming Strategy (JPA 에서는 HibernatePersistence 를 사용해야 합니다)
        // props.put(HibernatePersistence.NAMING_STRATEGY, getNamingStrategy());

        return props;
    }

    @Bean
    public EntityManagerFactory entityManagerFactory() throws IOException {
        log.info("EntityManagerFacotry 를 생성합니다...");

        LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();

        String[] packageNames = getMappedPackageNames();

        if (packageNames != null && packageNames.length > 0) {
            log.debug("JPA용 entity를 scan합니다. packages=[{}]", StringUtils.arrayToCommaDelimitedString(packageNames));
            factoryBean.setPackagesToScan(packageNames);
        }

        // factoryBean.setJpaPropertyMap(jpaProperties.getProperties());
        factoryBean.setJpaProperties(jpaProperties());
        factoryBean.setDataSource(dataSource());

        HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
        adapter.setGenerateDdl(true);
        factoryBean.setJpaVendorAdapter(adapter);

        factoryBean.afterPropertiesSet();
        log.info("EntityManagerFactory Bean 을 생성했습니다!!!");

        return factoryBean.getObject();
    }

    @Bean
    public PlatformTransactionManager transactionManager() throws IOException {
        return new JpaTransactionManager(entityManagerFactory());
    }

    @Bean
    public HibernateExceptionTranslator hibernateExceptionTranslator() {
        return new HibernateExceptionTranslator();
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslationPostProcessor() {
        return new PersistenceExceptionTranslationPostProcessor();
    }


}
