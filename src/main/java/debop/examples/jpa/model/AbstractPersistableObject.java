/*
 * Copyright (c) 2017. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package debop.examples.jpa.model;

import debop.examples.jpa.utils.ToStringHelper;

/**
 * AbstractPersistableObject
 *
 * @author sunghyouk.bae@gmail.com
 * @since 17. 11. 10
 */
public abstract class AbstractPersistableObject extends AbstractValueObject implements PersistableObject {

    private boolean persisted;

    @Override
    public boolean isPersisted() {
        return persisted;
    }

    protected void setPersisted(Boolean v) {
        persisted = v;
    }

    @Override
    public void onSave() {
        persisted = true;
    }

    @Override
    public void onLoad() {
        persisted = true;
    }

    @Override protected ToStringHelper buildStringHelper() {
        return super.buildStringHelper()
                    .add("persisted", persisted);
    }

    private static final long serialVersionUID = -2349850459160319985L;
}
