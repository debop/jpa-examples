/*
 * Copyright (c) 2017. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package debop.examples.jpa.model;

import debop.examples.jpa.utils.ToStringHelper;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.util.Objects;

/**
 * 계층형 구조 엔티티의 TREE 상에서의 위치를 나타냅니다.
 *
 * @author sunghyouk.bae@gmail.com
 * @since 17. 11. 10
 */
@Embeddable
@Getter
@Setter
public class TreeNodePosition extends AbstractValueObject {

    @Column(name = "treeLevel")
    private int level;

    @Column(name = "treeOrder")
    private int order;

    public TreeNodePosition() {
        this(0, 0);
    }

    public TreeNodePosition(int level, int order) {
        this.level = level;
        this.order = order;
    }

    @Override public int hashCode() {
        return Objects.hash(level, order);
    }

    @Override protected ToStringHelper buildStringHelper() {
        return super.buildStringHelper()
                    .add("level", level)
                    .add("order", order);
    }

    private static final long serialVersionUID = 4089621982101602646L;
}
