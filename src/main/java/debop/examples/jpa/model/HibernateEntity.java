/*
 * Copyright (c) 2017. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package debop.examples.jpa.model;

import java.io.Serializable;

/**
 * Hibernate, JPA 용 엔티티의 기본 인터페이스
 *
 * @author sunghyouk.bae@gmail.com
 * @since 17. 11. 10
 */
public interface HibernateEntity<TId extends Serializable> extends PersistableObject {

    /**
     * Hibernate Entity 의 Identifier 값을 나타냅니다.
     *
     * @return identifier value
     */
    TId getId();
}
