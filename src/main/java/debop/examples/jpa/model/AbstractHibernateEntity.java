/*
 * Copyright (c) 2017. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package debop.examples.jpa.model;

import debop.examples.jpa.utils.ToStringHelper;

import javax.persistence.MappedSuperclass;
import javax.persistence.PostLoad;
import javax.persistence.PostPersist;
import java.io.Serializable;

/**
 * Hibernate Entity의 최상위 추상화 클래스입니다.
 *
 * @author sunghyouk.bae@gmail.com
 * @since 17. 11. 10
 */
@MappedSuperclass
public abstract class AbstractHibernateEntity<TId extends Serializable> extends AbstractPersistableObject implements HibernateEntity<TId> {


    abstract public TId getId();

    // NOTE: JPA 에서는 #onSave(), #onLoad() 를 @MappedSuperclass 나 @Entity 에 재정의해야한다.
    // NOTE: Hibernate 에서는 Interceptor에서 관리하므로, {@link AbstractPersistentObject}에만 정의해도 된다.


    @Override
    @PostPersist
    public void onSave() {
        setPersisted(true);
    }

    @Override
    @PostLoad
    public void onLoad() {
        setPersisted(true);
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean equals(Object obj) {
        boolean isSampeType = (obj != null) && getClass().equals(obj.getClass());

        if (isSampeType) {
            HibernateEntity<TId> entity = (HibernateEntity<TId>) obj;
            return hasSameNonDefaultIdAs(entity) ||
                   ((!isPersisted() || !entity.isPersisted()) && hasSameBusinessSignature(entity));
        }
        return false;
    }

    private boolean hasSameNonDefaultIdAs(HibernateEntity<TId> entity) {
        if (entity == null) return false;

        TId id = getId();
        TId entityId = entity.getId();
        return (id != null) && (entityId != null) && (id.equals(entityId));
    }

    private boolean hasSameBusinessSignature(HibernateEntity<TId> other) {
        boolean notNull = (other != null);
        int hash = (getId() != null) ? getId().hashCode() : hashCode();
        if (notNull) {
            int otherHash = (other.getId() != null) ? other.getId().hashCode() : other.hashCode();
            return hash == otherHash;
        }
        return false;
    }

    @Override public int hashCode() {
        return (getId() == null)
               ? System.identityHashCode(this)
               : getId().hashCode();
    }

    @Override protected ToStringHelper buildStringHelper() {
        return super.buildStringHelper()
                    .add("id", getId());
    }

    private static final long serialVersionUID = 3578471725644480937L;
}
