/*
 * Copyright (c) 2017. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package debop.examples.jpa.utils;

import lombok.experimental.UtilityClass;

import java.util.*;
import java.util.function.Function;

@UtilityClass
public class GraphUtils {

    /**
     * Depth first search
     *
     * @param root     first node
     * @param adjacent retrieve function that adjacent node of current node
     * @param <T>      node type
     * @return collection of scanned node
     */
    public static <T> List<T> depthFirstSearch(T root, Function<T, Collection<? extends T>> adjacent) {
        assert root != null;
        assert adjacent != null;

        List<T> scanned = new ArrayList<>();
        Stack<T> toScan = new Stack<>();

        toScan.add(root);
        while (toScan.size() > 0) {
            T current = toScan.pop();
            scanned.add(current);
            toScan.addAll(adjacent.apply(current));
        }
        return scanned;
    }

    /**
     * Breadth first search
     *
     * @param root     first node
     * @param adjacent retrieve function that adjacent node of current node
     * @param <T>      node type
     * @return collection of scanned node
     */
    public static <T> List<T> breadthFirstSearch(T root, Function<T, Collection<? extends T>> adjacent) {
        assert root != null;
        assert adjacent != null;

        List<T> scanned = new ArrayList<>();
        ArrayDeque<T> toScan = new ArrayDeque<>();

        toScan.add(root);
        while (toScan.size() > 0) {
            T current = toScan.pop();
            scanned.add(current);
            toScan.addAll(adjacent.apply(current));
        }
        return scanned;
    }
}
