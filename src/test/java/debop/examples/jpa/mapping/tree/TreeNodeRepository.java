/*
 * Copyright (c) 2017. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package debop.examples.jpa.mapping.tree;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * TreeNodeRepository
 *
 * @author sunghyouk.bae@gmail.com
 * @since 17. 11. 10
 */
public interface TreeNodeRepository extends JpaRepository<TreeNode, Long> {

    @Query("select node from TreeNode node where node.parent is null")
    List<TreeNode> findRoots();

    List<TreeNode> findByParentIsNull();


    @Query("select node from TreeNode node where node.parent.id = :parentId")
    List<TreeNode> findByParent(@Param("parentId") Long parentId);

}
