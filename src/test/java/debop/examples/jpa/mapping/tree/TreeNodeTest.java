/*
 * Copyright (c) 2017. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package debop.examples.jpa.mapping.tree;

import debop.examples.jpa.AbstractJpaTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * TreeNodeTest
 *
 * @author sunghyouk.bae@gmail.com
 * @since 17. 11. 10
 */
@Slf4j
@Transactional
public class TreeNodeTest extends AbstractJpaTest {

    @PersistenceContext EntityManager em;
    @Autowired TreeNodeRepository repository;

    @Before
    public void setup() {
        repository.deleteAll();
    }

    @After
    public void cleanup() { }

    @Test
    public void contextLoads() {
        assertThat(em).isNotNull();
        assertThat(repository).isNotNull();
    }

    @Test
    public void testBuildTree() {

        TreeNode root = new TreeNode("root");

        TreeNode child1 = new TreeNode("child1");
        TreeNode child2 = new TreeNode("child2");

        root.addChild(child1);
        root.addChild(child2);

        TreeNode grandChild11 = new TreeNode("grandChild11");
        TreeNode grandChild12 = new TreeNode("grandChild12");

        child1.addChild(grandChild11);
        child1.addChild(grandChild12);

        repository.saveAndFlush(root);
        em.clear();  // for loading from repository not cache.

        log.debug("root={}", root);

        TreeNode node = repository.findOne(child1.getId());
        assertThat(node.getChildren().size()).isEqualTo(2);
        assertThat(node.getParent()).isEqualTo(root);

        em.clear();

        // query by HQL
        List<TreeNode> roots = repository.findRoots();
        assertThat(roots).hasSize(1);
        assertThat(roots.get(0)).isEqualTo(root);

        em.clear();

        // query by spring-data-jpa
        roots = repository.findByParentIsNull();
        assertThat(roots).hasSize(1);
        assertThat(roots.get(0)).isEqualTo(root);

        // query by parent id
        List<TreeNode> children = repository.findByParent(root.getId());
        assertThat(children).hasSize(2);
        assertThat(children).contains(child1, child2);
    }

    @Test
    public void testBuildTreeAndDelete() throws Exception {

        TreeNode root = new TreeNode("root");

        TreeNode child1 = new TreeNode("child1");
        TreeNode child2 = new TreeNode("child2");

        root.addChild(child1);
        root.addChild(child2);

        TreeNode grandChild11 = new TreeNode("grandChild11");
        TreeNode grandChild12 = new TreeNode("grandChild12");

        child1.addChild(grandChild11);
        child1.addChild(grandChild12);

        repository.saveAndFlush(root);
        em.clear();  // for loading from repository not cache.

        // delete child1 
        TreeNode node = repository.findOne(child1.getId());
        repository.delete(node);
        repository.flush();
        em.clear();

        // child1, grandChild11, grandChild12 must be deleted
        assertThat(repository.findOne(child1.getId())).isNull();
        assertThat(repository.findOne(grandChild11.getId())).isNull();
        assertThat(repository.findOne(grandChild12.getId())).isNull();

        List<TreeNode> children = repository.findByParent(root.getId());
        assertThat(children).hasSize(1);
        assertThat(children).containsExactly(child2);
    }
}
