/*
 * Copyright (c) 2017. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package debop.examples.jpa.mapping.tree;

import debop.examples.jpa.model.AbstractHibernateEntity;
import debop.examples.jpa.model.HibernateTreeEntity;
import debop.examples.jpa.model.TreeNodePosition;
import debop.examples.jpa.utils.ToStringHelper;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.*;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

/**
 * TreeNode
 *
 * @author sunghyouk.bae@gmail.com
 * @since 17. 11. 10
 */
@Entity
@DynamicInsert
@DynamicUpdate
@Getter
@Setter
@NoArgsConstructor
public class TreeNode extends AbstractHibernateEntity<Long> implements HibernateTreeEntity<TreeNode> {

    public TreeNode(String word) {
        this.word = word;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.PROTECTED)
    private Long id;

    private String word;
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @LazyToOne(LazyToOneOption.PROXY)
    @JoinColumn(name = "parent_id")
    private TreeNode parent;

    @OneToMany(mappedBy = "parent", cascade = {CascadeType.ALL}, orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.EXTRA)
    private Set<TreeNode> children = new LinkedHashSet<TreeNode>();

    TreeNodePosition nodePosition = new TreeNodePosition();

    @Override
    public void addChild(TreeNode child) {
        if (child != null) {
            children.add(child);
            child.setParent(this);
        }
    }

    @Override
    public void removeChild(TreeNode child) {
        if (child != null && children.contains(child)) {
            children.remove(child);
            child.setParent(null);
        }
    }

    @Override public int hashCode() {
        return Objects.hashCode(word);
    }

    @Override protected ToStringHelper buildStringHelper() {
        return super.buildStringHelper()
                    .add("word", word)
                    .add("descroption", description);
    }

    private static final long serialVersionUID = -2616814143692597957L;
}
