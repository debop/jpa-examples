/*
 * Copyright (c) 2017. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package debop.examples.jpa.utils;

import debop.examples.jpa.mapping.tree.TreeNode;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class GraphUtilsTest {

    private TreeNode root;
    private TreeNode child1, child2;
    private TreeNode grandChild11, grandChild12;

    @Before
    public void setup() {
        root = new TreeNode();
        root.setWord("root");

        child1 = new TreeNode();
        child1.setWord("child1");
        child2 = new TreeNode();
        child1.setWord("child2");

        grandChild11 = new TreeNode();
        grandChild11.setWord("grandChild11");
        grandChild12 = new TreeNode();
        grandChild12.setWord("grandChild12");

        root.addChild(child1);
        root.addChild(child2);

        child1.addChild(grandChild11);
        child1.addChild(grandChild12);
    }

    @Test
    public void test_depthFirstSearch() {
        List<TreeNode> scanned = GraphUtils.depthFirstSearch(root, TreeNode::getChildren);
        assertThat(scanned).containsExactlyInAnyOrder(root, child1, grandChild11, grandChild12, child2);
    }

    @Test
    public void test_breadthFirstSearch() {
        List<TreeNode> scanned = GraphUtils.breadthFirstSearch(root, TreeNode::getChildren);
        assertThat(scanned).containsExactlyInAnyOrder(root, child1, child2, grandChild11, grandChild12);
    }
}
