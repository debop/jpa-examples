/*
 * Copyright (c) 2017. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package debop.examples.jpa.config;

import debop.examples.jpa.mapping.tree.TreeNode;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * JpaH2Configuration
 *
 * @author sunghyouk.bae@gmail.com
 * @since 17. 11. 10
 */
@Configuration
@EnableJpaRepositories(basePackages = "debop.examples.jpa.mapping")
@EnableTransactionManagement(proxyTargetClass = true)
public class JpaH2Configuration extends AbstractH2JpaConfiguration {


    @Override public String[] getMappedPackageNames() {
        return new String[]{
                TreeNode.class.getPackage().getName()
        };
    }
}
